document.addEventListener("DOMContentLoaded", function() {
    var loadmore = document.getElementById("loadmore");
    var defaultLoadmoreText = loadmore.textContent;
    loadmore.style.display = "block";
    var postsgrid = document.getElementById("postsgrid");

    if(postsgrid.querySelector(".featured") === null) {
	var first = postsgrid.querySelector(".grid-post:first-child");
        if (first !== null) first.classList.add("mobile-pad-top");
    }

    if (loadmore !== undefined && postsgrid !== undefined) {
        var currentPage = 1;
        loadmore.addEventListener("click", function() {
            currentPage = loadPosts(currentPage);
        });
    }
    window.addEventListener('scroll', onScroll, false);

    function onScroll() {
        if(currentPage === 1 && window.scrollY > 50) {
            console.log('loading more posts', currentPage);
            currentPage = loadPosts(currentPage);
        }
    }
    function loadPosts(currentPage) {
        var req = new XMLHttpRequest();
        var nextPage = currentPage + 1;
        var url = ghost.url.api('posts', {
            limit: 6,
            page: nextPage
        });
        req.open('GET', url, true);
        loadmore.textContent = "Loading...";
        req.addEventListener("load", function() {
            if(req.status <= 200 && req.status < 400) {
                var resp = JSON.parse(req.responseText);
                if(resp.posts.length == 0) {
                    loadmore.textContent = "No more posts";
                    loadmore.style.pointerEvents = "none";
                    loadmore.style.opacity = 0.8;
                } else {
                    loadmore.textContent = defaultLoadmoreText;
                }
                resp.posts.forEach(function(postData) {
                    postsgrid.appendChild(renderPost(postData));
                });
            } else {
                console.error("Could not load more posts");
            }
        });

        req.send();
        return currentPage + 1;
    }
});


function renderPost(postData) {
    if ('content' in document.createElement('template')) {
        var t = document.getElementById('posttemplate');
        if(t) {
            tc = t.content;
            title = tc.querySelector(".grid-post__title > a");
            author = tc.querySelector(".grid-post__author");
            time = tc.querySelector(".grid-post__date");
            tags = tc.querySelector(".grid-post__tags");
            img = tc.querySelector(".grid-post__cover-image")

            title.textContent = postData.title;
            title.href = postData.url;

            author.textContent = "John Todo"; //TODO
            
            postDateEls = new Date(postData.created_at).toLocaleDateString('en-US').split("/");
            time.textContent = postDateEls.join('.');
            time.setAttribute("datetime", postDateEls.concat(postDateEls.splice(0,2)).join("-") );
            img.src = postData.feature_image;

            clone = document.importNode(tc, true);
            return clone;

        }
    }
}
