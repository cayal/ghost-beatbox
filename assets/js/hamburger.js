function addClass(el, className) {
    if (el.classList) {
        el.classList.add(className);
    }
    else {
        el.className += ' ' + className;
    }
}
function removeClass(el, className) {
    if (el.classList) {
        el.classList.remove(className);
    }
    else {
        el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
    }
}
function hasClass(el, className) {
    if (el.classList) {
        return el.classList.contains(className);
    }
    else {
        return new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className);
    }
}
function classToggle(el, classNameOne, classNameTwo) {
        if(hasClass(el, classNameOne)) {
            addClass(el, classNameTwo);
            removeClass(el, classNameOne);
        } else if(hasClass(classNameTwo)) {
            removeClass(el, classNameTwo);
            addClass(el, classNameOne);
        } else {
            console.warn("Can't toggle " + classNameTwo + classNameOne + " on " + el);
            return false;
        }
}
document.addEventListener('DOMContentLoaded', function(){
    var hamb = document.getElementById('mobile-header__sidebar-button');
    var wrap = document.getElementById('site-wrapper');
    var mhdr = document.getElementById('mobile-header');
    var lastScrollPos = 0;
    var ticking = false;

    hamb.addEventListener('click', function(e) {
        classToggle(hamb, "openable", "closable");
        classToggle(wrap, "sidebar-open", "sidebar-closed")
    });

    document.addEventListener('scroll', function() {
        lastScrollPos = window.scrollY;
        if (!ticking) {
            window.requestAnimationFrame(function() {
            shadeHeader(lastScrollPos, mhdr);
                ticking = false;
            });
        }
        ticking = true;
    })
    
});

function shadeHeader(scrollPos, header) {
    if(scrollPos > 100) {
        addClass(header, "shaded");
    } else {
        removeClass(header, "shaded");
    }
}